package com.example.assignment_12.model

import kotlinx.serialization.SerialName


data class testing(
    val id: String?,
    val projectId: String?,
    val equipmentId: String?,
    val status: String?,
    val requestedBy: String?,
    val author: String?,
    val category: String?,
//    val locations: Locations?,
    //   val filters: List<Filter>?,
    val type: String?,
    val organization: String?,
    val address: String?,
    val startDate: String?,
    val endDate: String?,
    val description: String?,
    val prolongDates: List<String>?,
    val releaseDates: List<String>?,
    val isDummy: Boolean?,
    val hasDriver: Boolean?,
    val overwriteDate: String?,
    val metaInfo: String?,
    val warehouseId: String?,
    val rentalDescription: String?,
    //  val internalTransportations: InternalTransportations?,
    val startDateMilliseconds: Long?,
    val endDateMilliseconds: Long?,
//    val equipment: Equipment?,
    val acceptedBy: String?,
) {
    data class Locations(
        val coordinates: List<Double>?,
        val type: String?
    )

    data class Filter(
        val name: String?,
        val value: Value?
    ) {
        data class Value(
            val max: Int?,
            val min: Int?
        )
    }


    data class InternalTransportations(
        val id: String?,
        val projectRequestId: String?,
        val pickUpDate: String?,
        val deliveryDate: String?,
        val description: String?,
        val status: String?,
        val startDateOption: Boolean?,
        val endDateOption: Boolean?,
        val pickUpLocation: PickUpLocation?,
        val deliveryLocation: DeliveryLocation?,
        val provider: String?,
        val pickUpLocationAddress: String?,
        val deliveryLocationAddress: String?,
        val pGroup: String?,
        val isOrganizedWithoutSam: String?,
        val templatePGroup: String?,
        val pickUpDateMilliseconds: Long?,
        val deliveryDateMilliseconds: Long?,
        val startDateOptionMilliseconds: Long?,
        val endDateOptionMilliseconds: Long?,
    ) {

        data class PickUpLocation(
            val coordinates: List<Double>?,
            val type: String?
        )

        data class DeliveryLocation(
            val coordinates: List<Double>?,
            val type: String?
        )
    }

    data class Equipment(
        val id: String?,
        val title: String?,
        val invNumber: String?,
        val categoryId: String?,
        val modelId: String?,
        val brandId: String?,
        val year: Int?,
        val specifications: List<Specification>?,
        val weight: Int?,
        @SerialName("additional_specifications")
        val additionalSpecifications: String?,
        val structureId: String?,
        val organizationId: String?,
        val beaconType: String?,
        val beaconId: String?,
        val beaconVendor: String?,
        val RFID: String?,
        val dailyPrice: Int?,
        val inactive: Boolean?,
        val tag: Tag?,
        val telematicBox: String?,
        val createdAt: String?,
        @SerialName("special_number")
        val specialNumber: String?,
        @SerialName("last_check")
        val lastCheck: String?,
        @SerialName("next_check")
        val nextCheck: String?,
        @SerialName("responsible_person")
        val responsiblePerson: String?,
        @SerialName("test_type")
        val testType: String?,
        @SerialName("unique_equipment_id")
        val uniqueEquipmentId: String?,
        @SerialName("bgl_number")
        val bglNumber: String?,
        val inventory: String?,
        val warehouseId: String?,
        val trackingTag: String?,
        val workingHours: String?,
        @SerialName("navaris_criteria")
        val navarisCriteria: String?,
        @SerialName("dont_send_to_as400")
        val dontSendToAs400: String?,
        val model: Model?,
        val brand: Brand?,
        val category: Category?,
        val structure: Structure?,
        val wareHouse: String?,
        val equipmentMedia: List<EquipmentMedia>?,
        val telematics: List<Telematic>?,
        val isMoving: Boolean?,
    ) {

        data class Specification(
            val key: String?,
            val value: String?
        )

        data class Tag(
            val date: String?,
            val authorName: String?,
            val media: List<String>?,
        )

        data class Model(
            val id: String?,
            val name: String?,
            val createdAt: String?,
            val brand: BrandX?,
        ) {

            data class BrandX(
                val id: String?,
                val name: String?,
                val createdAt: String?,
            )

        }

        data class Brand(
            val id: String?,
            val name: String?,
            val createdAt: String?,
        )

        data class Category(
            val id: String?,
            val name: String?,
            val name_de: String?,
            val createdAt: String?,
            val media: List<String>?,
        )

        data class Structure(
            val id: String?,
            val name: String?,
            val type: String?,
            val color: String?,
        )

        data class EquipmentMedia(
            val id: String?,
            val name: String?,
            val files: List<File>?,
            val type: String?,
            val modelId: String?,
            val main: Boolean?,
            val modelType: String?,
            val createdAt: String?,
        ) {

            data class File(
                val size: String?,
                val path: String?,
            )

        }

        data class Telematic(
            val timestamp: Long?,
            val eventType: String?,
            val projectId: String?,
            val equipmentId: String?,
            val locationName: String?,
            val location: Location?,
            val costCenter: String?,
            val lastLatitude: Double?,
            val lastLongitude: Double?,
            val lastLatLonPrecise: Boolean?,
            val lastAddressByLatLon: String?,
        ) {

            data class Location(
                val type: String?,
                val coordinates: List<List<List<List<Double>>>>?,
            )

        }
    }

}
