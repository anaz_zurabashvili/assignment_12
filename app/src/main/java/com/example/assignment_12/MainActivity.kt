package com.example.assignment_12

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.activity.viewModels
import com.example.assignment_12.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val item: ItemViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }
    private fun init(){
        d("checking", "${item.getJsonData()?.id}")
        binding.tvId.text = item.getJsonData()?.id
        binding.tvStatus.text = item.getJsonData()?.equipment?.equipmentMedia.toString()
    }
}